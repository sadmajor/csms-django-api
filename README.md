# Setup virtualenv

```bash
virtualenv .env -p python3.8
```

## Active virtualenv

```bash
source .env/bin/activate
```

## Install new package

use `pip-compile` to install packages

- install `pip-compile`

  ```bash
  pip install pip-tools
  ```

- install new package:
  1. add package name and version in `requirements.in` like the following:
     ```bash
     django==3.2.5
     ```
     if you want to find latest version just run `pip install django==`
  2. run the following command to update `requirements.txt`
     ```bash
     pip-compile --output-file=requirements.txt requirements.in
     ```
  3. install package by the following command
     ```bash
     pip install -r requirements.txt
     ```

# Run code on a machine with docker

1. build image (**Remember to build new image everytime you update the code**)

```bash
docker build -t csms_django_api .
```

2. run a container

```bash
docker run -p 8000:80 csms_django_api
```

3. test codes

```bash
python manage.py test
```

4. check api

```bash
curl \
    -d '{"rate": { "energy": 0.3, "time": 2, "transaction": 1 }, "cdr": { "meterStart": 1204307, "timestampStart": "2021-04-05T10:04:00Z", "meterStop": 1215230, "timestampStop": "2021-04-05T11:27:00Z" }}' \
    -H 'Content-Type: application/json' \
    -X POST http://localhost:8000/v1/cdr/rate/
```
