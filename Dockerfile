FROM python:3.8

WORKDIR /django-api

ADD ./requirements.txt ./
RUN pip install -r ./requirements.txt
ADD ./ ./
# run pycodestyle to make sure our code style is standard 
RUN pycodestyle
ENTRYPOINT [ "/bin/sh", "/django-api/entry.sh" ]
CMD [ "web" ]