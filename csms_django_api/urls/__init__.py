from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include

urlpatterns = [
    path("admin/", admin.site.urls),
    url(r"^v1/", include("csms_django_api.urls.v1")),
]
