from django.urls import include, path

urlpatterns = [
    path("cdr/", include("cdr.urls")),
]
