import json
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

# initialize the APIClient app
client = Client()


class RateTest(TestCase):
    """Test module for checking rate api"""

    def setUp(self):
        self.valid_payload = {
            "rate": {"energy": 0.3, "time": 2, "transaction": 1},
            "cdr": {
                "meterStart": 1204307,
                "timestampStart": "2021-04-05T10:04:00Z",
                "meterStop": 1215230,
                "timestampStop": "2021-04-05T11:27:00Z",
            },
        }
        self.valid_response = {
            "overall": 7.04,
            "components": {"energy": 3.277, "time": 2.767, "transaction": 1.0},
        }

    def test_rate_api(self):
        response = client.post(
            reverse("rate"), data=json.dumps(self.valid_payload), content_type="application/json"
        )
        self.assertEqual(response.data, self.valid_response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
