import re
from django.shortcuts import render
from rest_framework import serializers, viewsets
from rest_framework.response import Response
from cdr.serializers import RequestSerializer

# Create your views here.


class RateViewSet(viewsets.ViewSet):
    """
    This class represents rate related views.
    -functions
        result: call on post request.
    """

    def result(self, request):
        """
        This function calculate overall cost with details.
        -response
            overall: total cost [rounded to 2 decimal places]
            components:
                energy: energy cost [rounded to 3 decimal places]
                time: time cost [rounded to 3 decimal places]
                transaction: transaction cost [rounded to 3 decimal places]
        """
        serializer = RequestSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.validated_data
            # convert Wh to kWh by / 1000
            energy = round(
                (data["cdr"]["meterStop"] - data["cdr"]["meterStart"])
                * data["rate"]["energy"]
                / 1000,
                3,
            )
            # convert timedelta to hours by / 3600
            time = round(
                (data["cdr"]["timestampStop"] - data["cdr"]["timestampStart"]).seconds
                / 3600
                * data["rate"]["time"],
                3,
            )
            transaction = data["rate"]["transaction"]
            return Response(
                {
                    "overall": round(energy + time + transaction, 2),
                    "components": {
                        "energy": energy,
                        "time": time,
                        "transaction": round(transaction, 3),
                    },
                }
            )

        else:
            return Response(serializer.errors, status=400)
