from django.urls import path

from cdr.views import RateViewSet

urlpatterns = [
    path("rate/", RateViewSet.as_view({"post": "result"}), name="rate"),
]
