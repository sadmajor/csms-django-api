from rest_framework import serializers


class RateSerializer(serializers.Serializer):
    energy = serializers.FloatField()
    time = serializers.FloatField()
    transaction = serializers.FloatField()


class CdrSerializer(serializers.Serializer):
    meterStart = serializers.IntegerField(min_value=0)
    timestampStart = serializers.DateTimeField()
    meterStop = serializers.IntegerField(min_value=0)
    timestampStop = serializers.DateTimeField()

    def validate(self, data):
        if data["meterStart"] > data["meterStop"]:
            raise serializers.ValidationError(
                "meterStop must be greater than or equal to meterStart"
            )

        if data["timestampStart"] > data["timestampStop"]:
            raise serializers.ValidationError(
                "timestampStop must be greater than or equal to timestampStart"
            )

        return data


class RequestSerializer(serializers.Serializer):
    rate = RateSerializer()
    cdr = CdrSerializer()
